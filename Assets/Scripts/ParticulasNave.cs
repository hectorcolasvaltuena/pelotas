﻿using UnityEngine;
using System.Collections;

public class ParticulasNave : MonoBehaviour {
	public static float factor = 50.0f;
	public ParticleSystem emisor;

	public void SetColor (Color color) {
		emisor.startColor = color;
	}

	public void Emitir (float daño) {
		emisor.Emit((int)Mathf.Ceil(daño * factor));
	}
}
