﻿using UnityEngine;
using System.Collections;

public class NaveMasterProfanada : NaveMaster {
	/*
	public GameObject spawner;
	public GameObject objetivo;
	public Vector3 direccionSpawner;
	public Vector3 direccionObjetivo;
	public Vector2 aleatorio;
	public float rango = 10f;
	public float suavidad = 0.05f;
	public SpriteRenderer renderer;
	public Rigidbody2D rigidbody;
	public Color colorBase;
	public Global.Equipo equipo;

	// Use this for initialization
	void Start () {

		// SeleccionarSpawnerObjetivo ();

		rigidbody.gravityScale = 0f;

		renderer.material.color = new Color (colorBase.r, Random.value, colorBase.b);

		// Posicionamos aleatoriamente en un rango a lo largo de X Z
		aleatorio = Random.insideUnitCircle;
		// aleatorio = new Vector3 (aleatorio.x, 0, aleatorio.z);
		transform.position = spawner.transform.position + (Vector3)aleatorio*rango;

		// Hacemos que siempre miren hacia fuera de la base
		direccionSpawner = spawner.transform.position - transform.position;
		transform.right = -direccionSpawner;

	}

	void Update ( ) {
		direccionObjetivo = objetivo.transform.position - transform.position;

		// Hacemos Lerp desde nuestra forward hacia nuestra dirección objetivo
		// transform.forward = direccionObjetivo;
		transform.right = Vector3.Lerp ( transform.right , direccionObjetivo , suavidad );
	}

	void OnCollisionEnter2D ( ) {
		
		rigidbody.gravityScale += 0.1f;

		if (rigidbody.gravityScale > 1f) {
			Destroy (gameObject);
		}

	}

	public void SeleccionarSpawnerObjetivo ( ) {
		SpawnerMaster spawnerScript;
		do {
			spawnerScript = ControlNavesMaster.main.ObtenerSpawnerAleatorio ();
		} while (spawnerScript.equipo == equipo);
		objetivo = spawnerScript.gameObject;
	}
*/
	private static float modDañoCol = 0.01f;
	private static float minDañoCol = 0.1f;

	public float saludInicial = 1.0f;
	public ParticulasNave emisor;

	//private float salud;

	public float tamañoBase = 2.5f;
	private bool destruir = false;

	public override void Start () {
		base.Start();
		//salud = saludInicial;
		emisor.SetColor(renderer.color);
	}

	public override void Update () {
		if (destruir) Destroy(gameObject);
		base.Update ();
		transform.localScale = Vector3.one * tamañoBase * (1 - rigidbody.gravityScale);
		ComprobarSalud ();
	//	rigidbody.mass
	}

	public void ComprobarSalud () {
		if (rigidbody.gravityScale > 1f) {
			destruir = true;
			GetComponent<Collider2D>().enabled = false;
		}
	}

	public override void OnCollisionEnter2D (Collision2D coll) {
		float daño;
		if (coll.gameObject.tag != gameObject.tag) {
			daño = (((coll.rigidbody.velocity - rigidbody.velocity).magnitude) * modDañoCol);
			if (daño < minDañoCol) daño = minDañoCol;
			Debug.Log(daño);
			ColisionEnemigo(daño);
			emisor.Emitir(daño);
		}
	}
}
