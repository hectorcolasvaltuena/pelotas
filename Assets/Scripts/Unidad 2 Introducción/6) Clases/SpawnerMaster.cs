﻿using UnityEngine;
using System.Collections;

public class SpawnerMaster : MonoBehaviour {

	public float aleatorio;
	public float aleatorio2;
	public bool algunaTecla;
	public float pi;
	public float infinito;

	public Vector3 vector;
	public GameObject nave;
	private GameObject naveClon;
	public Global.Equipo equipo;
	public Color colorBase;
	public NaveMaster scriptNaveMaster;

	// Update is called once per frame
	public virtual void Update () {
		
		aleatorio = Random.value;
		algunaTecla = Input.anyKeyDown;
		pi = Mathf.PI;
		infinito = Mathf.Infinity;
		// colorBase = Color.magenta;
		vector = Vector3.forward;

		aleatorio2 = Random.Range (1.0f, 2.0f);

		if ( algunaTecla ) {
			SpawnearNave ();
		}
	}

	public void SpawnearNave () {
		naveClon = Instantiate (nave) as GameObject;
		naveClon.tag = gameObject.tag;
		naveClon.transform.parent = transform;
		scriptNaveMaster = naveClon.GetComponent<NaveMaster> ();
		scriptNaveMaster.equipo = equipo;
		scriptNaveMaster.colorBase = colorBase;
		scriptNaveMaster.spawner = gameObject;
		scriptNaveMaster.SeleccionarSpawnerObjetivo ();
	}
}
