﻿using UnityEngine;
using System.Collections;

public class NaveMaster : MonoBehaviour {

	public GameObject spawner;
	public GameObject objetivo;
	public Vector3 direccionSpawner;
	public Vector3 direccionObjetivo;
	public Vector2 aleatorio;
	public float rango = 10f;
	public float suavidad = 0.05f;
	public SpriteRenderer renderer;
	public Rigidbody2D rigidbody;
	public Color colorBase;
	public Global.Equipo equipo;

	// Use this for initialization
	public virtual void Start () {

		// SeleccionarSpawnerObjetivo ();

		rigidbody.gravityScale = 0f;

		renderer.color = new Color (colorBase.r, Random.value, colorBase.b);

		// Posicionamos aleatoriamente en un rango a lo largo de X Z
		aleatorio = Random.insideUnitCircle;
		// aleatorio = new Vector3 (aleatorio.x, 0, aleatorio.z);
		transform.position = spawner.transform.position + (Vector3)aleatorio*rango;

		// Hacemos que siempre miren hacia fuera de la base
		direccionSpawner = spawner.transform.position - transform.position;
		transform.right = -direccionSpawner;

	}

	public virtual void Update ( ) {
		direccionObjetivo = objetivo.transform.position - transform.position;

		// Hacemos Lerp desde nuestra forward hacia nuestra dirección objetivo
		// transform.forward = direccionObjetivo;
		transform.right = Vector3.Lerp ( transform.right , direccionObjetivo , suavidad );
	}

	public virtual void OnCollisionEnter2D (Collision2D coll) {
		ColisionEnemigo(0.1f);

	}

	public void ColisionEnemigo (float fuerza) {
		rigidbody.gravityScale += fuerza;

		/*if (rigidbody.gravityScale > 1f) {
			Destroy (gameObject);
		}*/
	}

	public void SeleccionarSpawnerObjetivo ( ) {
		SpawnerMaster spawnerScript;
		do {
			spawnerScript = ControlNavesMaster.main.ObtenerSpawnerAleatorio ();
		} while (spawnerScript.equipo == equipo);
		objetivo = spawnerScript.gameObject;
	}

}
