﻿using UnityEngine;
using System.Collections;

public class ControlNavesMaster : MonoBehaviour {

	public static ControlNavesMaster main;

	public SpawnerMaster [] spawners;

	// Update is called once per frame
	void Awake () {
		main = this;
		spawners = GameObject.FindObjectsOfType<SpawnerMaster> ();
	}

	public SpawnerMaster ObtenerSpawnerAleatorio ( ) {
		return spawners [Random.Range (0, spawners.Length)];
	}

}
